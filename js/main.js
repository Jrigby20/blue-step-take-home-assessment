

const name = document.getElementById("name")
const email = document.getElementById("email")
const password = document.getElementById("password")
const age = document.getElementById("age")
const phone = document.getElementById("phone")
const address = document.getElementById("address")
const dob = document.getElementById("dob")

const error = document.getElementById("error")
const form = document.getElementById("form")


const handleSubmit = async () => {
    data = {
        "name": name.value,
        "email": email.value,
        "password": password.value,
        "age": age.value,
        "phone": phone.value,
        "address": address.value,
        "dob": dob.value
    }
    const url = "myfunapi.fake/user/signup";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
};
console.log(data) //not stringified (not JSON)
console.log(JSON.stringify(data)) //stringified (converted to Json) as it is on the fetchconfig
// await fetch(url, fetchConfig); //commented out post request since url API is fake
const newUserSuccess = await mockAPI(JSON.stringify(data))
if (newUserSuccess["result"]){
    alert(newUserSuccess["message"])
}
else {
    alert("Error while creating a new user", newUserSuccess["message"])
}
console.log(newUserSuccess)

}

const mockAPI = async (jsonData) => {
    const data = await JSON.parse(jsonData)
    // let newUser= UserHelper.create()    //commented out because it's not a real library
    // let dataSet = newUser.setName(data.name)
    // dataSet = newUser.setEmail(data.email)
    // dataSet = newUser.setPassword(data.password)
    // dataSet = newUser.setAge(data.age)
    // dataSet = newUser.setPhone(data.phone)
    // dataSet = newUser.setAddress(data.address)
    // dataSet = newUser.setDob(data.dob)
    if (data.email === "" || data.name === ""){
        console.log("Name and Email are required fields")
        return {
            "message": "Name and Email are required fields",
            "result": false
        }
    }

    // saveSuccessful = UserHelper.save()
    // if(resultSuccessful){
    // return {"result": true, "message": "New User Created Successfully"} } // saveSuccessful should be either true or false according to docs
    // else{
    // return  {"result": false, "message": "Error Creating a new user"}
    //}
    return {
        "message": "New User Created Successfully",
        "result": true
    }

}

form.addEventListener('submit', (e) => {
    e.preventDefault()
    let errorMessages = []
if (password.value.length < 7 || password.value.length > 15){
    errorMessages.push('*Password must be between 7 and 15 characters')
}
if (errorMessages.length > 0){
    error.innerText = errorMessages.join(', ')
}
else{
    handleSubmit()
}
})
